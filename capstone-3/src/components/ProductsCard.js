import PropTypes from 'prop-types';
import React from 'react'
import { Card, Container, Button } from 'react-bootstrap';
import CardGroup from 'react-bootstrap/CardGroup'
import { useNavigate } from 'react-router-dom';
import UserContext from '../UserContext'
import { useContext } from 'react';


export default function Product({ productprop }) {
	const history = useNavigate()
	const { user } = useContext(UserContext)
	const addItemToCart = () => {
		console.log(user)
		if (user === null || user.id === null) {
			return history('/login')
		}
		let token = localStorage.getItem('token')
		fetch(`${process.env.REACT_APP_API_URL}/orders/add-to-cart`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${token}`
			},
			body: JSON.stringify({
				productId: _id,
				quantity: 1
			})
		})
			.then(result => result.json())
			.then(result => {

				history('/cart')
			})
	}
	const { name, description, price, _id } = productprop

	return (

		<Container className="card-container mx-3 d-inline-block product-card" xs={12} md={6}>
			<CardGroup>
				<Card.Body className="cards text-white">
					<Card.Title className="heading-highlights">{name}</Card.Title>
					<h5>Description:</h5>
					<p>{description}</p>
					<h5>Price:</h5>
					<p>₱ {price}</p>

					<Button className="btnprod" variant="outline-secondary" size="mx" onClick={addItemToCart}>
						Add to cart
					</Button>
				</Card.Body>
			</CardGroup>
		</Container>
	)
}

Product.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
