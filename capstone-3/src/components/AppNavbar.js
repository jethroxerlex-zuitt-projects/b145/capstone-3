import { useContext } from 'react'
import { Fragment } from 'react'
import { Navbar, Nav } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import UserContext from '../UserContext'
import React from 'react'


export default function AppNavbar() {

  const { user } = useContext(UserContext)
  console.log(user)

  return (
    <Navbar bg="transparent" expand="lg" className="navbar1">
      <Navbar.Brand href="/">
        <img
          src="/BTS.png"
          width="50"
          height="50"
          className="d-inline-block align-top mx-3"
        />
      </Navbar.Brand>
      <Navbar.Toggle />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto mainNav">
          <Nav.Link as={Link} to="/">Home</Nav.Link>
          <Nav.Link as={Link} to="/products">Products</Nav.Link>

          {(user.id !== null) ?
            <Fragment>
              <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
              <Nav.Link as={Link} to="/cart">Cart</Nav.Link>
            </Fragment>

            :

            <Fragment>

              <Nav.Link as={Link} to="/login">Login</Nav.Link>
              <Nav.Link as={Link} to="/register">Register</Nav.Link>
            </Fragment>
          }

        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}