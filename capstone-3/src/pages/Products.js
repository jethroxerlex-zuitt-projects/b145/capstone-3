import { useState, useEffect, useContext } from 'react';
import React from 'react'
import { Container } from 'react-bootstrap'

import AdminPanel from './../components/AdminPanel.js';
import UserPanel from './../components/UserPanel.js';

import UserContext from './../UserContext';

export default function Products() {

	const [products, setProducts] = useState([]);

	const { user } = useContext(UserContext);


	const fetchData = () => {
		let token = localStorage.getItem('token')
		const productRoute = user.isAdmin ? 'all' : ''
		fetch(`${process.env.REACT_APP_API_URL}/products/${productRoute}`, {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${token}`
			}
		})
			.then(result => result.json())
			.then(result => {
				console.log(result)

				setProducts(result)
			})
	}

	useEffect(() => {
		fetchData()
	}, [])

	return (
		<Container className="p-4">
			{(user.isAdmin === true) ?
				<AdminPanel productData={products} fetchData={fetchData} />
				:
				<UserPanel productData={products} />
			}
		</Container>
	)
}
