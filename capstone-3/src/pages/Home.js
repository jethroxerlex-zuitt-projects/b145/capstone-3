import { Fragment } from 'react'
import Banner from '../components/Banner'
import React from 'react'
import Highlights from '../components/Highlights'

export default function Home() {

    const data = {

        title: "SHOP",
        content: "Good products for everyone, everywhere",
        destination: "/products",
        label: "Buy Now!"
    }

    return (
        <Fragment>
            <Banner />
        </Fragment>
    )
}